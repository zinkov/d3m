import os.path
from d3m import container, utils
from d3m.metadata import hyperparams, base as metadata_base
from d3m.primitive_interfaces import base, transformer

from . import __author__, __version__

__all__ = ('FailPrimitive',)


# It is useful to define these names, so that you can reuse it both
# for class type arguments and method signatures.
Inputs = container.DataFrame
Outputs = container.DataFrame


class Hyperparams(hyperparams.Hyperparams):

    method_to_fail = hyperparams.Enumeration[str](
        values=['__init__', 'set_training_data', 'fit', 'produce'],
        default='produce',
        semantic_types=['https://metadata.datadrivendiscovery.org/types/TuningParameter'],
        description='The name of the method the user wants this primitive to fail on'
    )

class IntentionalError(Exception):
    """Exception raised for testing purposes.

    Attributes:
        class_name -- name of the class where the error occurred
        method_name -- name of the method where the error occurred
    """

    def __init__(self, class_name, method_name):
        message = 'This is an exception raised by a(n) {} object in the {} method'.format(class_name, method_name)
        super().__init__(message)

class FailPrimitive(transformer.TransformerPrimitiveBase[Inputs, Outputs, Hyperparams]):
    # It is important to provide a docstring because this docstring is used as a description of
    # a primitive. Some callers might analyze it to determine the nature and purpose of a primitive.

    """
    A primitive which fails on the requested method (given in hyperparam).
    """

    # This should contain only metadata which cannot be automatically determined from the code.
    metadata = metadata_base.PrimitiveMetadata({
        # Simply an UUID generated once and fixed forever. Generated using "uuid.uuid4()".
        'id': 'd6dfbefa-0fb8-11e9-ab14-d663bd873d93',
        'version': __version__,
        'name': "Failure Tester",
        # Keywords do not have a controlled vocabulary. Authors can put here whatever they find suitable.
        'keywords': ['test primitive'],
        'source': {
            'name': __author__,
            'contact': 'mailto:author@example.com',
            'uris': [
                # Unstructured URIs. Link to file and link to repo in this case.
                'https://gitlab.com/datadrivendiscovery/tests-data/blob/master/primitives/test_primitives/fail_primitive.py',
                'https://gitlab.com/datadrivendiscovery/tests-data.git',
            ],
        },
        # A list of dependencies in order. These can be Python packages, system packages, or Docker images.
        # Of course Python packages can also have their own dependencies, but sometimes it is necessary to
        # install a Python package first to be even able to run setup.py of another package. Or you have
        # a dependency which is not on PyPi.
        'installation': [{
            'type': metadata_base.PrimitiveInstallationType.PIP,
            'package_uri': 'git+https://gitlab.com/datadrivendiscovery/tests-data.git@{git_commit}#egg=test_primitives&subdirectory=primitives'.format(
                git_commit=utils.current_git_commit(os.path.dirname(__file__)),
            ),
        }],
        # URIs at which one can obtain code for the primitive, if available.
        'location_uris': [
            'https://gitlab.com/datadrivendiscovery/tests-data/raw/{git_commit}/primitives/test_primitives/fail_primitive.py'.format(
                git_commit=utils.current_git_commit(os.path.dirname(__file__)),
            ),
        ],
        # The same path the primitive is registered with entry points in setup.py.
        'python_path': 'd3m.primitives.operator.null.FailTest',
        # Choose these from a controlled vocabulary in the schema. If anything is missing which would
        # best describe the primitive, make a merge request.
        'algorithm_types': [
            metadata_base.PrimitiveAlgorithmType.IDENTITY_FUNCTION,
        ],
        'primitive_family': metadata_base.PrimitiveFamily.OPERATOR,
        # A metafeature about preconditions required for this primitive to operate well.
    })

    def __init__(self, *, hyperparams: Hyperparams) -> None:
        super().__init__(hyperparams=hyperparams)
        self._conditional_fail('__init__')

    def _conditional_fail(self, method_name):
        if self.hyperparams['method_to_fail'] == method_name:
            raise IntentionalError(self.__class__.__name__, method_name)

    def set_training_data(self) -> None:
        self._conditional_fail('set_training_data')
        super().set_training_data()

    def fit(self, *, timeout: float = None, iterations: int = None) -> base.CallResult[None]:
        self._conditional_fail('fit')
        return super().fit(timeout=timeout, iterations=iterations)

    def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> base.CallResult[Outputs]:
        self._conditional_fail('produce')
        return base.CallResult(inputs)
